package video

import (
	"github.com/xfrr/goffmpeg/transcoder"
)

func Transcode(input string, output string) error {
	trans := new(transcoder.Transcoder)

	err := trans.Initialize(input, output)

	done := trans.Run(false)

	err = <-done

	return err
}
