/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package sensor

import (
	"github.com/golang/protobuf/ptypes"
	"github.com/stratoberry/go-gpsd"
	"gitlab.com/skyhuborg/tracker/internal/common"
	pb "gitlab.com/skyhuborg/tracker/internal/proto"
	"log"
	"sync"
	"time"
)

type GPSDSensor struct {
	Addr    string
	data    []interface{}
	session *gpsd.Session
}

//func (s *GPSDSensor) device() pb.Sensor {
//	return pb.Sensor_GPSD_GPS
//}

func (s *GPSDSensor) name() string {
	return pb.Sensor_name[int32(pb.Sensor_GPSD_GPS)]
}

func (s *GPSDSensor) read() []interface{} {
	data := s.data
	data = nil
	return data
}

var tpvPool = sync.Pool{
	New: func() interface{} { return new(pb.GPS_TPVReport) },
}

var skyPool = sync.Pool{
	New: func() interface{} { return new(pb.GPS_SKYReport) },
}

func (s *GPSDSensor) messageFilter(r interface{}) {
	switch report := r.(type) {
	case *gpsd.TPVReport:
		ts, _ := ptypes.TimestampProto(report.Time)

		// Set globals to make accessible via common
		common.Info.GpsTime = report.Time

		if report.Lon != 0 {
			common.Info.Longitude = report.Lon
		}

		if report.Lat != 0 {
			common.Info.Latitude = report.Lat
		}

		p := tpvPool.Get().(*pb.GPS_TPVReport)
		defer tpvPool.Put(p)
		p.Sensor = pb.Sensor_GPSD_GPS
		p.Class = report.Class
		p.Tag = report.Tag
		p.Mode = int32(report.Mode)
		p.Time = ts
		p.Ept = report.Ept
		p.Lat = report.Lat
		p.Lon = report.Lon
		p.Alt = report.Alt
		p.Epx = report.Epx
		p.Epy = report.Epy
		p.Epv = report.Epv
		p.Track = report.Track
		p.Speed = report.Speed
		p.Climb = report.Climb
		p.Epd = report.Epd
		p.Eps = report.Eps
		p.Epc = report.Epc

		s.data = append(s.data, p)
	case *gpsd.SKYReport:
		var (
			satellites []*pb.GPS_Satellite
		)

		for _, s := range report.Satellites {
			sat := &pb.GPS_Satellite{
				PRN:  s.PRN,
				Az:   s.Az,
				El:   s.El,
				Ss:   s.Ss,
				Used: s.Used,
			}
			satellites = append(satellites, sat)
		}

		ts, _ := ptypes.TimestampProto(report.Time)

		p := skyPool.Get().(*pb.GPS_SKYReport)
		defer skyPool.Put(p)
		p.Sensor = pb.Sensor_GPSD_GPS
		p.Class = report.Class
		p.Tag = report.Tag
		p.Device = report.Device
		p.Time = ts
		p.Xdop = report.Xdop
		p.Ydop = report.Ydop
		p.Vdop = report.Vdop
		p.Tdop = report.Tdop
		p.Hdop = report.Hdop
		p.Pdop = report.Pdop
		p.Gdop = report.Gdop
		p.Satellites = satellites

		s.data = append(s.data, p)
	case *gpsd.GSTReport:
		ts, _ := ptypes.TimestampProto(report.Time)
		p := &pb.GPS_GSTReport{
			Sensor: pb.Sensor_GPSD_GPS,
			Class:  report.Class,
			Tag:    report.Tag,
			Device: report.Device,
			Time:   ts,
			Rms:    report.Rms,
			Major:  report.Major,
			Minor:  report.Minor,
			Orient: report.Orient,
			Lat:    report.Lat,
			Lon:    report.Lon,
			Alt:    report.Alt,
		}
		s.data = append(s.data, p)
	case *gpsd.ATTReport:
		ts, _ := ptypes.TimestampProto(report.Time)
		p := &pb.GPS_ATTReport{
			Sensor:      pb.Sensor_GPSD_GPS,
			Class:       report.Class,
			Tag:         report.Tag,
			Device:      report.Device,
			Time:        ts,
			Heading:     report.Heading,
			MagSt:       report.MagSt,
			Pitch:       report.Pitch,
			PitchSt:     report.PitchSt,
			Yaw:         report.Yaw,
			YawSt:       report.YawSt,
			Roll:        report.Roll,
			RollSt:      report.RollSt,
			Dip:         report.Dip,
			MagLen:      report.MagLen,
			MagX:        report.MagX,
			MagY:        report.MagY,
			MagZ:        report.MagZ,
			AccLen:      report.AccLen,
			AccX:        report.AccX,
			AccY:        report.AccY,
			AccZ:        report.AccZ,
			GyroX:       report.GyroX,
			GyroY:       report.GyroY,
			Depth:       report.Depth,
			Temperature: report.Temperature,
		}
		s.data = append(s.data, p)
	case *gpsd.VERSIONReport:
		p := &pb.GPS_VERSIONReport{
			Sensor:     pb.Sensor_GPSD_GPS,
			Class:      report.Class,
			Release:    report.Release,
			Rev:        report.Rev,
			ProtoMajor: int32(report.ProtoMajor),
			ProtoMinor: int32(report.ProtoMinor),
			Remote:     report.Remote,
		}
		s.data = append(s.data, p)
	case *gpsd.DEVICESReport:
		var (
			devices []*pb.GPS_DEVICEReport
		)

		for _, s := range report.Devices {
			dev := &pb.GPS_DEVICEReport{
				Sensor:    pb.Sensor_GPSD_GPS,
				Class:     s.Class,
				Path:      s.Path,
				Activated: s.Activated,
				Flags:     int32(s.Flags),
				Driver:    s.Driver,
				Subtype:   s.Subtype,
				Bps:       int32(s.Bps),
				Parity:    s.Parity,
				Stopbits:  s.Stopbits,
				Native:    int32(s.Native),
				Cycle:     s.Cycle,
				Mincycle:  s.Mincycle,
			}
			devices = append(devices, dev)
		}
		p := &pb.GPS_DEVICESReport{
			Sensor:  pb.Sensor_GPSD_GPS,
			Class:   report.Class,
			Devices: devices,
			Remote:  report.Remote,
		}
		s.data = append(s.data, p)
	case *gpsd.DEVICEReport:
		p := &pb.GPS_DEVICEReport{
			Sensor:    pb.Sensor_GPSD_GPS,
			Class:     report.Class,
			Path:      report.Path,
			Activated: report.Activated,
			Flags:     int32(report.Flags),
			Driver:    report.Driver,
			Subtype:   report.Subtype,
			Bps:       int32(report.Bps),
			Parity:    report.Parity,
			Stopbits:  report.Stopbits,
			Native:    int32(report.Native),
			Cycle:     report.Cycle,
			Mincycle:  report.Mincycle,
		}
		s.data = append(s.data, p)
	case *gpsd.PPSReport:
		p := &pb.GPS_PPSReport{
			Sensor:     pb.Sensor_GPSD_GPS,
			Class:      report.Class,
			Device:     report.Device,
			RealSec:    report.RealSec,
			RealMusec:  report.RealMusec,
			ClockSec:   report.ClockSec,
			ClockMusec: report.ClockMusec,
		}
		s.data = append(s.data, p)
	case *gpsd.ERRORReport:
		p := &pb.GPS_ERRORReport{
			Sensor:  pb.Sensor_GPSD_GPS,
			Class:   report.Class,
			Message: report.Message,
		}
		s.data = append(s.data, p)
	case *gpsd.Satellite:
		p := &pb.GPS_Satellite{
			PRN:  report.PRN,
			Az:   report.Az,
			El:   report.El,
			Ss:   report.Ss,
			Used: report.Used,
		}
		s.data = append(s.data, p)
	}
}

func gpsgo(s *GPSDSensor) {
	done := s.session.Watch()
	<-done
}

func (s *GPSDSensor) init() bool {
	var (
		err         error
		nAttemps    int = 0
		maxAttempts int = 180
	)

	for {
		if nAttemps == 1 {
			log.Printf("Waiting for GPSD to come online\n")
		} else if nAttemps > maxAttempts {
			log.Fatalf("Failed connecting to GPSD at %s after %d seconds\n", s.Addr, maxAttempts)
			return false
		}

		s.session, err = gpsd.Dial(s.Addr)

		if err == nil {
			log.Printf("Successfully connected to GPSD at %s\n", s.Addr)
			break
		}

		time.Sleep(1 * time.Second)
		nAttemps++
	}

	s.session.AddFilter("VERSION", s.messageFilter)
	s.session.AddFilter("TPV", s.messageFilter)
	s.session.AddFilter("SKY", s.messageFilter)
	s.session.AddFilter("ATT", s.messageFilter)
	s.session.AddFilter("GST", s.messageFilter)
	s.session.AddFilter("PPS", s.messageFilter)
	s.session.AddFilter("Devices", s.messageFilter)
	s.session.AddFilter("DEVICE", s.messageFilter)
	s.session.AddFilter("ERROR", s.messageFilter)

	go gpsgo(s)

	return true
}

func (s *GPSDSensor) close() bool {
	return true
}
