#!/bin/sh
# id INTEGER PRIMARY KEY AUTOINCREMENT,
# event_id INTEGER PRIMARY KEY,
# event_id INTEGER DEFAULT NULL,
sqlite3 tracker.db <<EOF
DROP TABLE IF EXISTS events;

CREATE TABLE events (
	id CHAR(36) NOT NULL PRIMARY KEY,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
	started_at DATETIME,
	ended_at DATETIME,
	duration INT,
    type TEXT,
    source TEXT,
    sensor TEXT);

DROP TABLE IF EXISTS video_events;
CREATE TABLE video_events (
	event_id CHAR(36) DEFAULT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    uri TEXT,
    thumb TEXT);

DROP TABLE IF EXISTS sensors;
CREATE TABLE sensors (
	event_id CHAR(36) DEFAULT NULL,
	created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
	data BLOB
);
EOF
