package upload

import (
	"errors"
	"github.com/google/uuid"
	"net/http"
	"strings"
)

type Request struct {
	TrackerId string
	EventId   string
	Filename  string
}

func isValidUuid(u string) bool {
	_, err := uuid.Parse(u)

	return err == nil
}

func getEventIdFromFilename(fname string) string {
	a := strings.Split(fname, ".")

	if len(a) < 1 {
		return ""
	}

	return a[0]
}

func verifyHeaders(r *Request) error {
	if len(r.TrackerId) == 0 ||
		len(r.EventId) == 0 ||
		len(r.Filename) == 0 {
		return errors.New("missing field in headers")
	}

	// validate Tracker and event UUIDs
	if !isValidUuid(r.TrackerId) ||
		!isValidUuid(r.EventId) {
		return errors.New("failed verify UUID")
	}

	if getEventIdFromFilename(r.Filename) != r.EventId {
		return errors.New("failed processing filename")
	}
	return nil
}

func getRequest(header *http.Header) *Request {
	if header == nil {
		return nil
	}

	r := &Request{
		TrackerId: header.Get("TrackerId"),
		EventId:   header.Get("EventId"),
		Filename:  header.Get("Filename"),
	}
	return r
}

func setHeaders(header *http.Header, trackerId string, eventId string, filename string) bool {
	if header == nil {
		return false
	}

	if len(trackerId) == 0 ||
		len(eventId) == 0 ||
		len(filename) == 0 {
		return false
	}

	if !isValidUuid(trackerId) ||
		!isValidUuid(eventId) {
		return false
	}

	if getEventIdFromFilename(filename) != eventId {
		return false
	}

	header.Add("TrackerId", trackerId)
	header.Add("EventId", eventId)
	header.Add("Filename", filename)

	return true
}
