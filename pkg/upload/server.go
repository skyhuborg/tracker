package upload

import (
	"fmt"
	"gitlab.com/skyhuborg/tracker/internal/db"
	"io"
	"log"
	"net/http"
	"os"
)

type Server struct {
	Config *ServerConfig
	db     *db.DB
}

type ServerConfig struct {
	DataPath    string
	ListenPort  int
	EnableHttps bool
	DbUri       string
}

func NewServer(config *ServerConfig) *Server {
	server := &Server{
		Config: config,
		db:     db.NewDB(),
	}

	os.MkdirAll(config.DataPath, 0755)

	return server
}

func (s *Server) Start() {
	var (
		err error = s.db.Connect(s.Config.DbUri)
	)

	if err != nil {
		log.Fatalf("Db Connect failed with %s\n", err)
	}

	listenAddress := fmt.Sprintf(":%d", s.Config.ListenPort)
	http.HandleFunc("/UploadVideo", s.uploadVideoHandler)
	http.HandleFunc("/UploadThumbnail", s.uploadThumbnailHandler)

	http.ListenAndServe(listenAddress, nil)
}

/*
func (s *Server) trackerIdExists(uuid string) bool {
	var (
		exists bool = true
	)

	info := db.TrackerInfo{}
	info.Uuid = uuid

	result := s.db.Handle.First(&info)

	if result.Error != nil {
		return false
	}

	if result.RowsAffected < 1 {
		return false
	}

	return exists
}

func (s *Server) eventIdExists(uuid string) bool {
	var (
		exists bool = true
	)

	event := db.Event{}
	event.Uuid = uuid

	result := s.db.Handle.First(&event)

	if result.Error != nil {
		return false
	}

	if result.RowsAffected < 1 {
		return false
	}

	return exists
}*/

func (s *Server) verifyRequest(r Request) (err error) {
	err = verifyHeaders(&r)

	if err != nil {
		return
	}

	// make sure the tracker/event id exist in db
	/*
		if !s.trackerIdExists(r.TrackerId) {
			err = errors.New("Unknown TrackerId")
			return
		} else
		 if !s.eventIdExists(r.EventId) {
			err = errors.New("Unknown EventId")
			return
		}
	*/

	return
}

func (s *Server) uploadVideoHandler(w http.ResponseWriter, r *http.Request) {
	var (
		uri string
		err error
	)

	h := getRequest(&r.Header)

	err = s.verifyRequest(*h)

	if err == nil {
		uri = fmt.Sprintf("%s/%s", s.Config.DataPath, h.Filename)

		file, err := os.Create(uri)
		if err != nil {
			panic(err)
		}
		n, err := io.Copy(file, r.Body)
		if err != nil {
			panic(err)
		}
		log.Printf("Received Video for Event=%s Filename=%s Size=%d\n",
			h.EventId,
			h.Filename,
			n)

		if !s.db.SetVideoReceived(h.EventId, uri) {
			log.Printf("Failed setting VideoEvent.VideoReceived '%s' to true\n", h.EventId)
		}
	} else {
		log.Printf("Rejected Video for Event=%s Filename=%s\n",
			h.EventId,
			h.Filename)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("%s\n", err)))
	}
}

func (s *Server) uploadThumbnailHandler(w http.ResponseWriter, r *http.Request) {
	var (
		uri string
		err error
	)
	h := getRequest(&r.Header)

	err = s.verifyRequest(*h)

	if err == nil {
		uri = fmt.Sprintf("%s/%s", s.Config.DataPath, h.Filename)

		file, err := os.Create(uri)
		if err != nil {
			panic(err)
		}
		_, err = io.Copy(file, r.Body)
		if err != nil {
			panic(err)
		}
		fmt.Printf("Received Thumbnail for Event=%s Filename=%s\n",
			h.EventId,
			h.Filename)

		if !s.db.SetThumbnailReceived(h.EventId, uri) {
			log.Printf("Failed setting VideoEvent.ThumbnailRecieved '%s' to true\n", h.EventId)
		}
	} else {
		log.Printf("Rejected Thumbnail for Event=%s Filename=%s Size=%d\n",
			h.EventId,
			h.Filename)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("%s\n", err)))
	}
}
